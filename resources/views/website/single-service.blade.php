@extends('website.layouts.app')

@section('title', $service->name)

@section('description',  $service->short_description)

@section('keywords', $service->keywords)

@section('bread', $service->name)

@section('image', asset('storage/'. $service->logo))

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="title-2">
                    {{ $service->name }}
                </h1>
            </div>
            <div class="col-12">
                {!! $service->description !!}
            </div>
        </div>
    </div>

@endsection
